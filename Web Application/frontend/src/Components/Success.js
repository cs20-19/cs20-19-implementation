import React from "react";
import { motion } from "framer-motion";

const Success = ({ animatePage }) => {
  return (
    <motion.div className="success" {...animatePage}>
      Thank You {window.innerWidth < 768 && <br />}For Your Order!
    </motion.div>
  );
};

export default Success;
