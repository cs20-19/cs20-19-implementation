#include <wiringPi.h>
#include <softPwm.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#define PORT 6969

/**
Main function of this script keeps searching for an order.
After finding an order, the system searches for a pump holding the desired drink.
Then it checks the state of this pump (reservoir filled or other errors)
If the drink is available in the assigned pump, it will pour the drink.
After pouring, the process repeats itself.

Additional features to be added:
- drop cup
- notify car
- check if car is underneath
**/

const int pwmPin = 0;
const int in1 = 1;
const int in2 = 2;
const int dutyCycle = 50;
const int numberOfDrinks = 2;

// variables for the connection to the C++ server:
int sock = 0;
int valread;
char order;
struct sockaddr_in serv_addr;
char hello[] = "hd;";
char buffer[3];
bool successful = true;
bool orderReceived;

class drink {            // The class drink houses a drink identified by its drink ID and links this to the connected pump's motor pin.
  public:                // Access specifier
    int drinkNumber;     // ID of the drink
    int motorPin;        // Connected pin
    bool reservoirState; // State of the drink resevoir
    drink() {

    }
    drink(int dn, int mp, bool av) {
        drinkNumber = dn;
        motorPin = mp;
        reservoirState = av;
    }
    void set(int dn, int mp, bool av) {
        drinkNumber = dn;
        motorPin = mp;
        reservoirState = av;
    }
};

void dropCup() {    // The function dropCup() drops one cup at a time. When the function is called, it will send a PWM signal to the servo to allow one cup to drop.
    int PWM_pin = 7;    // The Pi pin used for the PWM signal for servo.
    int forLoop;        // A variable for the length of the loops.
    wiringPiSetup();
    pinMode(PWM_pin, OUTPUT);       // These functions set the PWM pin to output and set the range of the signal.
    softPwmCreate(PWM_pin,1,100);

    for (forLoop = 0; forLoop < 100; forLoop++) {
        softPwmWrite(PWM_pin, 1);   // This makes sure that the servo is in the begin position.
        delay(10);
    }
    delay(1);
    for (forLoop = 0; forLoop < 100; forLoop++) {
        softPwmWrite(PWM_pin, 10);  // This moves the servo to the position where it allows the cup to drop.
        delay(3);
    }
    delay(5);
    for (forLoop = 0; forLoop < 100; forLoop++) {
        softPwmWrite(PWM_pin, 1);   // This will move the servo back to the begin position.
        delay(10);
    }
}

int getOrderedDrink() { // get the drink ID of the ordered drink
    int iorder = -1;
    while (iorder != 1 && iorder != 2) {
        read(sock, buffer, 3);
        iorder = (int) buffer[0]-47;
    }
    return iorder;
}

void notifyServer(int status) {
    if(status == 1) {
        send(sock, "con", strlen("con") , 0);
    }
    else {
        send(sock, "err", strlen("err") , 0);
    }
}

drink getDrinkLocation(int orderedDrink, drink drinkArray[], int i) { // finds the motor pin associated with the desired drink
    for(i; i < numberOfDrinks; i++) {
        if(orderedDrink == drinkArray[i].drinkNumber) { // return drink that matches the ordered drink ID
            return drinkArray[i];
        }
    }
    drink drinkNotAvailable(-1, -2, 0); // case the drink does not match any available drinks
    return drinkNotAvailable;
}

int getArrayLocation(int orderedDrink, drink drinkArray[], int i) { // finds the motor pin associated with the desired drink
    for(i; i < numberOfDrinks; i++) {
        if(orderedDrink == drinkArray[i].drinkNumber) { // return indice of the drinkArray that matches the orderedDrink
            return i;
        }
    }
    return 0;
}

bool checkAvailabilityDrink(drink drinkLocation) { // check availability of the drink
    return drinkLocation.reservoirState;
}

void pourDrink(drink drinkLocation) { // electrically drive the motor of the desired drink
    digitalWrite(drinkLocation.motorPin, HIGH);
    usleep(7000000);
    digitalWrite(drinkLocation.motorPin, LOW);
    usleep(3000000);

}

void notifyUnavailable() { // notifies the user that the drink is unavailable, possible communication implementation with webapp.
    printf("drink unavailable\n");
}

// findAvailablePump finds a pump using getDrinkLocation. Then it runs a check whether it is available, 
// if not it will continue searching for a different pump that potentially has the same drink.
// After going through every pump, it will forward this information as well.
drink findAvailablePump(int orderedDrink, drink drinkArray[]) { 
    int i = 0;
    drink drinkLocation(-1, -1, 0);
    //loop while orderedDrink has not been found AND reservoir is unavailable
    while(drinkLocation.drinkNumber != orderedDrink || !drinkLocation.reservoirState) { 
        drinkLocation = getDrinkLocation(orderedDrink, drinkArray, i); // find pump linked to the ordered drink
        if(drinkLocation.motorPin == -2) { // if we reach the end of the drinkArray, break the loop
            break;
        } else if (!drinkLocation.reservoirState) { // if the end of the drinkArray has not yet been reached AND reservoir is still unavailable
            i = getArrayLocation(orderedDrink, drinkArray, i) + 1; // update starting indice and search again
            if (i == numberOfDrinks) { // avoid case that next drink does not exist.
                break;
            }
        }
    }
    return drinkLocation;
}

/**
The function (connectToServer()) below is based on an online example on GeeksForGeeks, https://www.geeksforgeeks.org/socket-programming-cc/ on 29-10-2020. 
The code is used to test how we can connect the C++ script to the server. Once this works and we understand everything,
we will make our own code that will use the right messages and protocol.
**/
bool connectToServer() { 

printf("Looking for server.\n");
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        successful = false;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, "192.168.1.211", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        successful = false;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) <0) {
        printf("\nConnection Failed \n");
        successful = false;
    }

    valread = read(sock , buffer, 1024);
    printf("%s\n", buffer);
    
    send(sock, hello, strlen(hello), 0);

    printf("Hello message sent.\n");
    
    if (successful) {
        return 1;
    }

    return 0;
}


int main() {
    wiringPiSetup();			// Setup the library
    softPwmCreate(pwmPin, 0, 100); // Configure GPIO17 as software PWM pin
    softPwmWrite(pwmPin, dutyCycle);
    pinMode(in1, OUTPUT);		// Configure GPIO18, first pump
    pinMode(in2, OUTPUT);       // Configure GPIO27, second pump
    int orderedDrink = -1;      // drink ID of the ordered drink
    bool connected = 0;
    drink drinkArray[numberOfDrinks];       // Stocked drinks array
    drinkArray[0].set(1, in1, 1);
    drinkArray[1].set(2, in2, 1);

    // connect with server
   connected = connectToServer();

    while(connected) // main program loop
    {
        // add queueing system for orders here later if we have time
        orderedDrink = getOrderedDrink(); // find an ordered drink from the web app
        drink drinkLocation(-1, -1, 0); // initialise drinkLocation
        drinkLocation = findAvailablePump(orderedDrink, drinkArray); // find a pump which has the ordered drink on stock

        if(checkAvailabilityDrink(drinkLocation)) { // if an ordered drink location is available
            dropCup(); // drop cup
            usleep(4000000); // delay between drop cup and pour
            pourDrink(drinkLocation); // turn on the pump to pour the drink
            notifyServer(1);
            printf("All ready signal sent.");
        } else {
            notifyServer(0);
            printf("Error message sent.");
        }

        orderedDrink = -1; // reset the ordered drink value

    }

    printf("No connection with server.\n");

        return 0;
}