#include <wiringPi.h>
#include <pthread.h>
#include <stdio.h>
#include <time.h>

#define triggerpin 22
#define echopin 21

void distance(){
    for (;;){
        digitalWrite(triggerpin,HIGH);
        delay(100);
        digitalWrite(triggerpin,LOW);

        //Stopwatch starts here
        clock_t start,end;
        start = clock();

        while(digitalRead(echopin)==LOW){
            //printf("signal low\n");
            start = clock();
        }
        while(digitalRead(echopin)==HIGH){
            end = clock();
        }
        float duration = ((float) end - start)/CLOCKS_PER_SEC;
        //printf("Duration: %3f\n", duration);
        float distance = ((duration * 34300) / 2);
        printf("Distance: %3f\n",distance);
        delay(100);
    }
}

int main(int argc, char * argv[]){
    wiringPiSetup();
    pinMode(triggerpin, OUTPUT);
    pinMode(echopin, INPUT);
    digitalWrite(triggerpin, LOW);
    distance();
    return 0 ;
}