//Hosts website on http://localhost:8081
//REQUIRES other server to route orders to.
const express = require('express');
const app = express();
const http = require('http');
const fs = require('fs');
const bodyParser = require('body-parser');
const { constants } = require('crypto');
const net = require('net');
const cors = require('cors');
const { send } = require('process');

const tableCount = 2;
var drinks = JSON.parse(fs.readFileSync("drinks.json"));
app.use(express.json());//Parses request body as JSON
app.use(cors());  	

var dispenser;
var car;

//Sets up the server to
console.log('Attempting to setup server.');
const Server = net.createServer((c) => {
   var isDispenser = true;

   console.log('a client connected');

   if(dispenser && car){
      c.write('ful');
      c.end();
   } else c.write('hs;');
   console.log('hello message sent');

   c.on('data', async (s) =>{
      let response = s.toString('utf8', 0, 3);
      console.log('message recieved: ' + response);
      if(response === 'hc;') car = c;
      if(response === 'hd;') dispenser = c;
   });

   c.on('end', () => {
      if(c === dispenser) dispenser = null;
      if(c === car) car = null;
      console.log('a client disconnected');
   });
   c.pipe(c);
})

Server.on('error', (err) => {
  console.log('Unknown error occured ¯\\\_(ツ)_/¯');
});
Server.listen(6969, () => {
  console.log('Server started.');
});

//Checks if a drink id is allowed
function checkDrink(id){
   for(drink in drinks){
      if(drinks[drink].id == id) return true
   }
   return false;
}

//Sends a message to the dispenser to dispense a drink, followed by a message to the car to go there, followed by a message to the car to return.
function sendDrink(id, table){
   console.log("Will serve: %s.", id);
   var waitTime;
   switch(table) {   //Sets the time the system needs to wait for the car to arrive and wait for the customer to pick up the drink. This is hardcoded in because having it wait for the system to send a confirmation would require us to rewrite the entire page.
      case '1': waitTime = 55; break;
      case '2': waitTime = 65; break;
      default: waitTime = 0;
   }
   console.log(typeof table);
   if(!sendMessage(id + ';;', 'dispenser', 17)){
      console.log('Dispenser failed');
      return false;
   }
   if(!sendMessage(table + ';;', 'car', waitTime)){
      console.log('Car did not reach table');
      return false;
   }
   if(!sendMessage('0;;', 'car', 105-waitTime)){
     console.log('Car did not reach dispenser');
     return false;
   }
   return true;
}

//Sends a message to the car or dispenser, followed by waiting waitTime seconds.
async function sendMessage(message, target, waitTime){
   console.log('sending ' + message + ' to ' + target);
   if(target === 'car'){
      car.write(message);
      car.pipe(car);
   }
   else if (target === 'dispenser'){
      dispenser.write(message);
      dispenser.pipe(dispenser);
   } else return false;
   sleep(waitTime);
   return true;
}

//Sleep for x seconds
function sleep(seconds) {
   const date = Date.now();
   let currentDate = null; 
   do {
     currentDate = Date.now();
   } while (currentDate - date < seconds * 1000);
 }

//Part of the server listening for HTML requests

//Sends a Hello message
app.get('/hello', function (req, res) {
   res.send('Hello World');
})

//Sends the list of drinks to the frontend
app.get('/list', function (req, res) {
   res.json(drinks);
})

//Sends an order. Requires the body to contain a table object with a valid table number, and an array with one or more items that all contain an 'id' and 'count'.
app.post('/order', function (req, res) {
   console.log(req.body);
   var table = req.body.table;
   req.body.drinkOrder.forEach(drink => {
      let {count, id} = drink;
      if(table != undefined && id != undefined && checkDrink(id) && table >= tableCount){
         console.log(drink);
         for(let i = 1; i <= count; i++) {
            sendDrink(id, table);
         }
      } else {
         res.send('Failed');
         console.log('Faulty order recieved');
         return;
      }
   })
   console.log('Succeeded');
   res.send('Succeeded');
})

//Starts the backend server.
var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("App listening at http://%s:%s", host, port)
})