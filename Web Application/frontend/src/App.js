import React, { useState, useEffect } from "react";
import { AnimatePresence } from "framer-motion";
import { Route, Redirect, Switch, useLocation } from "react-router-dom";
import Basket from "./Components/Basket";
import Menu from "./Components/Menu";
import Name from "./Components/Name";
import Order from "./Components/Order";
import Success from "./Components/Success";
import "./css/style.css";

const App = () => {
  const animatePage = {
    variants: {
      initial: { x: "100vw", opacity: 0 },
      animate: { x: 0, opacity: 1 },
      transition: { duration: 0.4, type: "tween", delay: 0.4 },
      exit: { x: "-100vw", opacity: 0 },
    },
    initial: "initial",
    animate: "animate",
    transition: "transition",
    exit: "exit",
  };

  //localStorage.removeItem('drinks');
  const [drinksSelected, setDrinksSelected] = useState(
    localStorage.getItem("drinks") == null
      ? []
      : JSON.parse(localStorage.getItem("drinks"))
  );

  useEffect(() =>
    localStorage.setItem("drinks", JSON.stringify(drinksSelected))
  );

  const location = useLocation();

  return (
    <div
      className="App"
      style={{
        backgroundColor: window.location.href.toString().includes("success")
          ? "rgba(177, 0, 0, 1)"
          : "#fff",
      }}
    >
      <Name />
      <AnimatePresence exitBeforeEnter>
        <Switch location={location} key={location.key}>
          <Route path="/orderDrinks">
            {drinksSelected.length === 0 && <Redirect to="/" />}
            <Order
              animatePage={animatePage}
              setDrinksSelected={setDrinksSelected}
              drinksSelected={drinksSelected}
            />
          </Route>
          <Route path="/success">
            <Success animatePage={animatePage} />
          </Route>
          <Route path="/">
            <div>
              <Menu
                animatePage={animatePage}
                setDrinksSelected={setDrinksSelected}
                drinksSelected={drinksSelected}
              />
              <Basket
                animatePage={animatePage}
                setDrinksSelected={setDrinksSelected}
                drinksSelected={drinksSelected}
              />
            </div>
          </Route>
          <Route>
            <Redirect to="/" />
          </Route>
        </Switch>
      </AnimatePresence>
    </div>
  );
};

export default App;
