import DrinkList from "../data/drinks.json";

const useDrinksCalc = (drinksSelected) => {
  const drinkNames = DrinkList.map((drink) => drink.name);
  const drinksOrdered = {};
  drinkNames.forEach(
    (name) =>
      (drinksOrdered[name] = drinksSelected.filter(
        (drink) => drink.name === name
      ))
  );
  const drinkItemsTotal = [];
  for (let drinkOrdered in drinksOrdered) {
    drinkItemsTotal.push({
      name: drinkOrdered,
      orders: drinksOrdered[drinkOrdered],
    });
  }
  let sum =
    drinksSelected.length > 1
      ? Number(
          drinksSelected
            .map((drink) => parseFloat(drink.price))
            .reduce((a, b) => a + b)
        ).toFixed(2)
      : 0;
  return [drinkItemsTotal, sum];
};

export default useDrinksCalc;
