import React from 'react'

const Name = () => {
    return (
        <div className="name">
            <div className="name__restaurant">
                Restaurant Name
            </div>
            <div className="name__autendant">
                Autendant
            </div>
        </div> 
    )
}

export default Name
