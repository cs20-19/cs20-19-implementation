#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <pthread.h>

class Ultrasonic { 
public:
    static int trigger, echo;
    static pthread_t* init(int trigger, int echo);
    static void *IRreceiver(void *ptr);
    static void *IRTransmitter(void *ptr);
};