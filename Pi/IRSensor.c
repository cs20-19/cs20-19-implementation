#include <wiringPi.h>
#include <pthread.h>
#include <stdio.h>
#define IRT 4  //Transmitter
#define IRR 5  //Receiver
#define IRR 6  //Receiver


int detection = HIGH; //No obstacles

void IRreceiver(){
    for(;;){
        detection = digitalRead(IRR);
        if(detection==LOW){
            printf("IRR = LOW\n");
        }
        else{
            printf("IRR = HIGH\n");
        }
        delay(150);
    }
    
}

void IRTransmitter(){
    for(;;){
        digitalWrite(IRT, HIGH);
        delay(50);
        digitalWrite(IRT, LOW);
        delay(50);  

    }
    
}

int main(int argc, char * argv[]){
    wiringPiSetup();
    pinMode(IRT, OUTPUT);
    pinMode(IRR, INPUT);
    pthread_t thread[2];
    pthread_create(&thread[0], NULL, (void *) &IRreceiver, NULL);
    pthread_create(&thread[1], NULL, (void *) &IRTransmitter, NULL); 
    pthread_join(thread[0], NULL);
    pthread_join(thread[1], NULL);
    return 0 ;
}
