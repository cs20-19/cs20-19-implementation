#include <stdio.h>
#include <wiringPi.h>
#include <pthread.h>
#include <csignal>
#include <cstdlib>
#include "InfraredManager.h"
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fstream>

#define pin_motorA1 0 //#GPIO17
#define pin_motorA2 1 //#GPIO18
#define pin_motorB1 2 //#GPIO27
#define pin_motorB2 3 //#GPIO22
#define pin_irtR 4    //#GPIO23
#define pin_irtL 7    //#GPIO04
#define pin_irrR 5    //#GPIO24
#define pin_irrL 6    //#GPIO25

#define FORWARD 0
#define RIGHT 1
#define LEFT 2
#define STOP 3

#define MAX_CHECKPOINT 2
#define DISPENSER_CHECKPOINT 0

#define PORT 6969

int current_checkpoint = 0;
int target_checkpoint = -1;
int can_move = 0;


void signalHandler(int signum)
{
    digitalWrite(pin_motorA1, LOW);
    digitalWrite(pin_motorA2, LOW);
    digitalWrite(pin_motorB1, LOW);
    digitalWrite(pin_motorB2, LOW);

    exit(EXIT_FAILURE);
}

int previous_direction = STOP;
int direction = STOP;
int on = 0;
int can_change_current_checkpoint = 1;

void *directionLoop(void *ptr)
{
    while (1)
    {
        if (on)
        {
            digitalWrite(pin_motorA1, LOW);
            digitalWrite(pin_motorA2, LOW);
            digitalWrite(pin_motorB1, LOW);
            digitalWrite(pin_motorB2, LOW);
            on = 0;
            delay(40);
        }
        else
        {
            if (can_move) {
                if (direction == FORWARD)
                {
                    //printf("forward.\n");
                    digitalWrite(pin_motorA1, HIGH);
                    digitalWrite(pin_motorA2, LOW);
                    digitalWrite(pin_motorB1, HIGH);
                    digitalWrite(pin_motorB2, LOW);
                }
                else if (direction == RIGHT)
                {
                    //printf("right.\n");
                    digitalWrite(pin_motorA1, LOW);
                    digitalWrite(pin_motorA2, HIGH);
                    digitalWrite(pin_motorB1, HIGH);
                    digitalWrite(pin_motorB2, LOW);
                }
                else if (direction == LEFT)
                {
                    //printf("left.\n");
                    digitalWrite(pin_motorA1, HIGH);
                    digitalWrite(pin_motorA2, LOW);
                    digitalWrite(pin_motorB1, LOW);
                    digitalWrite(pin_motorB2, HIGH);
                }
                else
                {
                    //printf("stop.\n");
                    digitalWrite(pin_motorA1, HIGH);
                    digitalWrite(pin_motorA2, LOW);
                    digitalWrite(pin_motorB1, HIGH);
                    digitalWrite(pin_motorB2, LOW);
                }
                on = 1;
            } else {
                digitalWrite(pin_motorA1, LOW);
                digitalWrite(pin_motorA2, LOW);
                digitalWrite(pin_motorB1, LOW);
                digitalWrite(pin_motorB2, LOW);
                //printf("Is waiting on checkpoint.\n");
            }
        }
        if(direction == LEFT || direction == RIGHT){
            delay(10);
        }
        delay(20);
    }
    return NULL;
}

void *networking(void *ptr){
    char *hello = "hc;";

    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};
    bool success = true;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\nSocket creation error.\n");
        success = false;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, "192.168.1.211", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address.\n");
        success = false;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) <0) {
        printf("\nConnection Failed.\n");
        success = false;
    }

    //printf("Networking successful: %d\n", success);
    
    send(sock, hello, strlen(hello) , 0);
    printf("Hello message sent\n");

    while (1) {
        valread = read(sock, buffer, 1024);
        printf("Received: %s\n", buffer);
        if (strcmp(buffer, "i;;") == 0) {
            exit(EXIT_FAILURE);
        } else if (strcmp(buffer, "hs;") == 0 || strcmp(buffer, "hc;") == 0) {

        } else if (buffer[2] == ';'){
            int id = buffer[0] - '0';
            printf("WHAT: %s\n", buffer);
            printf("Checkpoint to go to: %d\n" + id);
            target_checkpoint = id;
            can_move = 1;
        }
    }
    
    
    return NULL;
}

void *letItChange(void *ptr) {
    delay(3000);
    can_change_current_checkpoint = 1;
    return NULL;
}

int main()
{
    //Initializes and sets up wiringPi.
    wiringPiSetup();

    //Set mode to out.
    //Wheel A: direction control.
    pinMode(pin_motorA1, OUTPUT); //#GPIO17
    pinMode(pin_motorA2, OUTPUT); //#GPIO18
    //Wheel B: direction control.
    pinMode(pin_motorB1, OUTPUT); //#GPIO27
    pinMode(pin_motorB2, OUTPUT); //#GPIO22

    signal(SIGINT, signalHandler);

    //Setup IR

    pthread_t *irThread = InfraredManager::init(pin_irtL, pin_irtR, pin_irrR, pin_irrL);
    pthread_t direction_control;
    pthread_create(&direction_control, NULL, directionLoop, NULL);
    pthread_t networkingT;
    pthread_create(&networkingT, NULL, networking, NULL);

    delay(500);
    while (true)
    {
        if (can_move){
            if (!InfraredManager::right_detection && !InfraredManager::left_detection)
            {
                direction = FORWARD;
                if (direction != previous_direction) {
                    printf("forward.\n");
                }
            }
            else if (InfraredManager::right_detection && !InfraredManager::left_detection)
            {
                direction = RIGHT;
                if (direction != previous_direction) {
                    printf("right.\n");
                }
            }
            else if (!InfraredManager::right_detection && InfraredManager::left_detection)
            {
                direction = LEFT;
                if (direction != previous_direction) {
                    printf("left.\n");
                }
            }
            else if (InfraredManager::right_detection && InfraredManager::left_detection)
            {
                direction = STOP;
                if (previous_direction != STOP) {
                    if (can_change_current_checkpoint) {
                        can_change_current_checkpoint = 0;

                        pthread_t t;
                        pthread_create(&t, NULL, letItChange, NULL);

                        if (current_checkpoint < MAX_CHECKPOINT) {
                            current_checkpoint ++;
                        } else {
                            current_checkpoint = 0;
                        }

                        printf("Passed: %d\n", current_checkpoint);

                        if (current_checkpoint == target_checkpoint) {
                            printf("papa %d\n", target_checkpoint);
                            target_checkpoint = -1;
                            can_move = 0;
                        } else {
                            printf("mama %d\n", target_checkpoint);
                        }
                    }
                    
                }
                if (direction != previous_direction) {
                    printf("Waiting at checkpoint for web-app instruction.\n");
                }
            }
            else
            {
                direction = STOP;
            }
            previous_direction = direction;
            delay(50);
            }
        
    }

    //Wait on IR threads before shutting down.
    pthread_join(*irThread, NULL);
    pthread_join(direction_control, NULL);
    pthread_join(networkingT, NULL);

    return 0;
}