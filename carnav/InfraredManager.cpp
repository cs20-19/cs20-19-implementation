#include "InfraredManager.h"

std::chrono::high_resolution_clock::time_point InfraredManager::startR;
std::chrono::high_resolution_clock::time_point InfraredManager::endR;
std::chrono::high_resolution_clock::time_point InfraredManager::startL;
std::chrono::high_resolution_clock::time_point InfraredManager::endL;

int InfraredManager::irtL = 0;
int InfraredManager::irtR = 0;
int InfraredManager::irrR = 0;
int InfraredManager::irrL = 0;

int InfraredManager::right_detection = 0;
int InfraredManager::left_detection = 0;

pthread_t* InfraredManager::init(int irtL, int irtR, int irrR, int irrL){
    InfraredManager::irtL = irtL;
    InfraredManager::irtR = irtR;
    InfraredManager::irrR = irrR;
    InfraredManager::irrL = irrL;

    printf("Attempting to initialize IR.\n");
    //IR transmitters
    pinMode(irtL, OUTPUT);
    pinMode(irtR, OUTPUT);
    //IR receivers
    pinMode(irrR, INPUT);
    pinMode(irrL, INPUT);

    digitalWrite(InfraredManager::irtL, LOW);
    digitalWrite(InfraredManager::irtR, LOW);
    printf("Initialized IR transmitters at GPIO: L  %d (OUT) and R %d (OUT), right IR receiver: %d (IN), left IR receiver: %d (IN)\n", InfraredManager::irtL, InfraredManager::irtR, InfraredManager::irrR, InfraredManager::irrL);
    

    pthread_t* thread = (pthread_t*)malloc(sizeof(pthread_t));
    pthread_create(thread, NULL, IRTransmitter, NULL);
    return thread;
}

void *InfraredManager::IRreceiver(void *ptr){
    delay(500);
    InfraredManager::startR = std::chrono::high_resolution_clock::now();
    while (1) {
        if (digitalRead(irrR) == 0) {
            InfraredManager::startR = std::chrono::high_resolution_clock::now();
        } else {
            InfraredManager::endR = std::chrono::high_resolution_clock::now();
        }

        if (digitalRead(irrL) == 0) {
            InfraredManager::startL = std::chrono::high_resolution_clock::now();
        } else {
            InfraredManager::endL = std::chrono::high_resolution_clock::now();
        }

        std::chrono::duration<double, std::milli> diffR = (endR - startR);
        std::chrono::duration<double, std::milli> diffL = (endL - startL);

        if (diffR.count() > 70) {
            InfraredManager::right_detection = 1;
            //printf("RIGHT!\n");
        } else {
            InfraredManager::right_detection = 0;
        }
        if (diffL.count() > 70) {
            InfraredManager::left_detection = 1;
            //printf("LEFT!\n");
        } else {
            InfraredManager::left_detection = 0;
        }
    }
    return NULL;
}

//Sends an IR Signal every 0.05 seconds
void *InfraredManager::IRTransmitter(void *ptr){
    pthread_t recv;
    pthread_create(&recv, NULL, IRreceiver, NULL);
    while (1) {
        digitalWrite(InfraredManager::irtL, HIGH);
        digitalWrite(InfraredManager::irtR, HIGH);
        delay(60);
        digitalWrite(InfraredManager::irtL, LOW);
        digitalWrite(InfraredManager::irtR, LOW);
        delay(60);
    }
    return NULL;
}