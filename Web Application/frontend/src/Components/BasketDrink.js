import React from "react";
import { motion, AnimatePresence } from "framer-motion";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const BasketDrink = ({
  drinkItemsType,
  setDrinksSelected,
  drinksSelected,
  animatePage,
  num,
}) => {
  let drinkName = drinkItemsType.name;
  let drinkCount = drinkItemsType.orders.length;

  return (
    <AnimatePresence>
      <motion.div
        className="drinkname"
        {...animatePage}
        transition={{
          delay: 0.4 + num * 0.05,
        }}
      >
        <div className="drinkname__count">
          {drinkName}
          {window.innerWidth >= 768 && " x "}
          {window.innerWidth < 768 && <br />}
          {drinkCount}
        </div>
        <button
          className="drinkname__remove"
          onClick={() => {
            let first = true;
            setDrinksSelected(
              drinksSelected.filter((item) => {
                if (drinkName === item.name) {
                  if (first) {
                    first = false;
                    return false;
                  }
                }
                return true;
              })
            );
          }}
        >
          {window.innerWidth > 768 && <span>Remove</span>}
          {window.innerWidth <= 768 && (
            <span>
              <FontAwesomeIcon icon={faTrashAlt} />
            </span>
          )}
        </button>
      </motion.div>
    </AnimatePresence>
  );
};

export default BasketDrink;
