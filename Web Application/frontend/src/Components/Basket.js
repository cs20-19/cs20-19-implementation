import React from "react";
import BasketDrinkType from "./BasketDrink";
import useDrinksCalc from "./useDrinksCalc";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";

const Basket = ({ setDrinksSelected, drinksSelected, animatePage }) => {
  const [drinkItemsTotal, sum] = useDrinksCalc(drinksSelected);
  return (
    <motion.div className="basket" {...animatePage}>
      <div className="basket__header">Basket</div>
      <div className="basket__orders">
        {drinksSelected.length === 0 && (
          <>{window.innerWidth > 768 && <span>No Orders Yet!</span>}</>
        )}
        {drinksSelected.length !== 0 &&
          drinkItemsTotal
            .filter((drinkSelectedItem) => drinkSelectedItem.orders.length > 0)
            .map((drinkItemsType, i) => (
              <BasketDrinkType
                num={i}
                animatePage={animatePage}
                key={drinkItemsType.name}
                drinkItemsType={drinkItemsType}
                setDrinksSelected={setDrinksSelected}
                drinksSelected={drinksSelected}
              />
            ))}
      </div>
      <hr className="divider"></hr>
      <div className="basket__bottom">
        <div className="basket__price">
          {drinksSelected.length === 0 && <>Total : $0.00</>}
          {drinksSelected.length === 1 && (
            <span>Total : ${drinksSelected[0].price}</span>
          )}
          {drinksSelected.length > 1 && <span>Total : ${sum}</span>}
        </div>
        <Link
          to="/orderDrinks"
          className="basket__confirm"
          style={{
            textAlign: "center",
            textDecoration: "none",
            pointerEvents: drinksSelected.length === 0 ? "none" : "auto",
          }}
        >
          Confirm Order
        </Link>
      </div>
    </motion.div>
  );
};

export default Basket;
