import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import DrinkList from "../data/drinks.json";
import Drink from "./DrinkMenu";
import { motion } from "framer-motion";

const Menu = ({ setDrinksSelected, drinksSelected, animatePage }) => {
  let settings = {
    centerMode: true,
    vertical: true,
    verticalSwiping: true,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <motion.div className="main" {...animatePage}>
      {window.innerWidth < 768 && (
        <Slider {...settings}>
          {DrinkList.map((drink, i) => (
            <Drink
              key={i}
              drink={drink}
              drinksSelected={drinksSelected}
              setDrinksSelected={setDrinksSelected}
            />
          ))}
        </Slider>
      )}
      {window.innerWidth >= 768 && (
        <>
          {DrinkList.map((drink, i) => (
            <Drink
              key={i}
              drink={drink}
              drinksSelected={drinksSelected}
              setDrinksSelected={setDrinksSelected}
            />
          ))}
        </>
      )}
    </motion.div>
  );
};

export default Menu;
