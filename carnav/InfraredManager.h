#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <pthread.h>
#include <chrono>
#include <time.h>
#include <ratio>

class InfraredManager { 
public:
    static std::chrono::high_resolution_clock::time_point startR, endR, startL, endL;
    static int irtL, irtR, irrR, irrL;
    static int right_detection;
    static int left_detection;
    static pthread_t* init(int irtL, int irtR, int irrR, int irrL);
    static void *IRreceiver(void *ptr);
    static void *IRTransmitter(void *ptr);
};