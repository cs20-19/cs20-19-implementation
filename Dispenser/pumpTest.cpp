#include <wiringPi.h>
#include <softPwm.h>
#include <stdio.h>
#include <unistd.h>

/**
Main function of this script keeps searching for an order.
After finding an order, the system searches for a pump holding the desired drink.
Then it checks the state of this pump (reservoir filled or other errors)
If the drink is available in the assigned pump, it will pour the drink.
After pouring, the process repeats itself.

Additional features to be added:
- drop cup
- notify car
- check if car is underneath
**/

const int pwmPin = 0;
const int in1 = 1;
const int in2 = 2;
const int dutyCycle = 40;
const int numberOfDrinks = 2;

class drink {       // The class drink houses a drink identified by its drink ID and links this to the connected pump's motor pin.
  public:             // Access specifier
    int drinkNumber;        // ID of the drink
    int motorPin; // Connected pin
    bool reservoirState; // State of the drink resevoir
    drink() {

    }
    drink(int dn, int mp, bool av) {
        drinkNumber = dn;
        motorPin = mp;
        reservoirState = av;
    }
    void set(int dn, int mp, bool av) {
        drinkNumber = dn;
        motorPin = mp;
        reservoirState = av;
    }
};

int getOrderedDrink() { // get the drink ID of the ordered drink
    // TO-DO: write a while loop to keep searching until it receives input from the web app
    //******** receive int from web app
}

drink getDrinkLocation(int orderedDrink, drink drinkArray[], int i) { // finds the motor pin associated with the desired drink
    for(i; i < numberOfDrinks; i++) {
        if(orderedDrink == drinkArray[i].drinkNumber) { // return drink that matches the ordered drink ID
            return drinkArray[i];
        }
    }
    drink drinkNotAvailable(-1, -2, 0); // case the drink does not match any available drinks
    return drinkNotAvailable;
}

int getArrayLocation(int orderedDrink, drink drinkArray[], int i) { // finds the motor pin associated with the desired drink
    for(i; i < numberOfDrinks; i++) {
        if(orderedDrink == drinkArray[i].drinkNumber) { // return indice of the drinkArray that matches the orderedDrink
            return i;
        }
    }
    return 0;
}

bool checkAvailabilityDrink(drink drinkLocation) { // check availability of the drink
    return drinkLocation.reservoirState;
}

void pourDrink(drink drinkLocation) { // electrically drive the motor of the desired drink
    digitalWrite(drinkLocation.motorPin, HIGH);
    usleep(7000000);
    digitalWrite(drinkLocation.motorPin, LOW);
    usleep(3000000);

}

void notifyUnavailable() { // notifies the user that the drink is unavailable, possible communication implementation with webapp.
    printf("drink unavailable\n");
}

// findAvailablePump finds a pump using getDrinkLocation. Then it runs a check whether it is available, 
// if not it will continue searching for a different pump that potentially has the same drink.
// After going through every pump, it will forward this information as well.
drink findAvailablePump(int orderedDrink, drink drinkArray[]) { 
    int i = 0;
    drink drinkLocation(-1, -1, 0);
    //loop while orderedDrink has not been found AND reservoir is unavailable
    while(drinkLocation.drinkNumber != orderedDrink && !drinkLocation.reservoirState) { 
        drinkLocation = getDrinkLocation(orderedDrink, drinkArray, i); // find pump linked to the ordered drink
        if(drinkLocation.motorPin == -2) { // if we reach the end of the drinkArray, break the loop
            break;
        } else if (!drinkLocation.reservoirState) { // if the end of the drinkArray has not yet been reached AND reservoir is still unavailable
            i = getArrayLocation(orderedDrink, drinkArray, i) + 1; // update starting indice and search again
            if (i == numberOfDrinks) { // avoid case that next drink does not exist.
                break;
            }
        }
    }
    return drinkLocation;
}


int main() // runs and loops the program
{
    wiringPiSetup();			// Setup the library
    softPwmCreate(pwmPin, 0, 100); // Configure GPIO17 as software PWM pin
    softPwmWrite(pwmPin, dutyCycle);
    pinMode(in1, OUTPUT);		// Configure GPIO18, first pump
    pinMode(in2, OUTPUT);       // Configure GPIO27, second pump
    int orderedDrink = -1;      // drink ID of the ordered drink
    drink drinkArray[numberOfDrinks];       // Stocked drinks array
    drinkArray[0].set(1, in1, 1);
    drinkArray[1].set(2, in2, 1);

        // add queueing system for orders here later if we have time
        orderedDrink = 2; // find an ordered drink from the web app
        usleep(5000000);
        drink drinkLocation(-1, -1, 0);
        drinkLocation = findAvailablePump(orderedDrink, drinkArray); // find a pump which has the ordered drink on stock

        if(checkAvailabilityDrink(drinkLocation)) { // if an ordered drink location is available
            // drop cup
            pourDrink(drinkLocation); // turn on the pump to pour the drink
            printf("%d \n", drinkLocation.motorPin);
            // notify car after pouring
        } else {
            notifyUnavailable();
        }

        orderedDrink = -1; // reset the ordered drink value

        return 0;
}