import React from "react";

const DrinkMenu = ({ drink, drinksSelected, setDrinksSelected }) => {
  return (
    <div className="drink">
      <div
        className="drink__img"
        style={{ backgroundImage: `url(${drink.image})` }}
      ></div>
      <div className="drink__name">{drink.name}</div>
      <div className="drink__price">{parseFloat(drink.price).toFixed(2)}</div>
      <button
        className="drink__order"
        onClick={() => setDrinksSelected([...drinksSelected, drink])}
      >
        Order
      </button>
    </div>
  );
};

export default DrinkMenu;
