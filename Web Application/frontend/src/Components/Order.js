import React from "react";
import useDrinksCalc from "./useDrinksCalc";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
import axios from "axios";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Order = ({ drinksSelected, animatePage }) => {
  const [drinkItemsTotal, sum] = useDrinksCalc(drinksSelected);
  const { register, handleSubmit } = useForm();

  let allOn = false;

  setTimeout(() => (allOn = true), 2000);

  const drinkOrder = drinkItemsTotal
    .map((drink) => {
      return {
        name: drink.name,
        count: drink.orders.length,
        ...drink.orders[0],
      };
    })
    .filter((drink) => drink.count !== 0);

  const format = (digit, name) => {
    let stringLen = 40;
    let stringOrg = `${digit}x${name}`;
    let itemSelected = drinkOrder.filter((drink) => drink.name === name)[0];
    let cost = itemSelected.count * itemSelected.price;
    let output =
      stringOrg +
      ".".repeat(
        stringLen - stringOrg.length - cost.toFixed(2).toString().length
      ) +
      cost.toFixed(2);
    return output;
  };

  const onSubmit = (data) => {
    const infoSent = {
      table: data.table,
      drinkOrder,
      sum,
    };
    if (data.table !== "") {
      console.log(infoSent);
      axios.post("http://127.0.0.1:8081/order", infoSent).then((res) => {
        if (res.data === "Succeeded") {
          window.location.href = window.location.href.replace(
            "orderDrinks",
            "success"
          );
        }
      });
    }
  };
  return (
    <div className="order">
      <motion.div className="order__upper" {...animatePage}>
        <motion.div
          {...animatePage}
          transition={{
            duration: allOn ? 0 : 0.4,
            type: "spring",
            delay: allOn ? 0 : 2,
          }}
        >
          <Link to="/">
            <FontAwesomeIcon icon={faChevronLeft} />
          </Link>
        </motion.div>
        <div>
          <div className="order__heading">List of Orders:</div>
          <div className="order__drinks">
            {drinkItemsTotal
              .filter(
                (drinkSelectedItem) => drinkSelectedItem.orders.length > 0
              )
              .map((drinkItemsType, i) => (
                <motion.div
                  key={drinkItemsType.name}
                  className="drinkname"
                  {...animatePage}
                  transition={{
                    duration: allOn ? 0 : 0.4,
                    type: "spring",
                    delay: allOn ? 0 : 0.4 + i * 0.05,
                  }}
                >
                  <div className="drinkname__count">
                    {format(drinkItemsType.orders.length, drinkItemsType.name)}
                  </div>
                </motion.div>
              ))}
          </div>
          <motion.div
            className="order__cost"
            {...animatePage}
            transition={{
              duration: allOn ? 0 : 0.4,
              type: "spring",
              delay: allOn ? 0 : 0.9,
            }}
          >
            <b>Cost:</b> ${sum}
          </motion.div>
        </div>
      </motion.div>
      <form onSubmit={handleSubmit(onSubmit)} className="order__form">
        <motion.div
          {...animatePage}
          transition={{
            duration: allOn ? 0 : 0.4,
            type: "spring",
            delay: 0,
          }}
        >
          <div>Order at Table: </div>
          <input ref={register} type="number" name="table" />
        </motion.div>
        <motion.button
          {...animatePage}
          type={"submit"}
          transition={{
            duration: allOn ? 0 : 0.4,
            type: "spring",
            delay: 0,
          }}
        >
          Order
        </motion.button>
      </form>
    </div>
  );
};

export default Order;
